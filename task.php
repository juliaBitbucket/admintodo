<?php
class Task {

    private $db;
    private $table = 'task';

    public function __construct()
    {
        $this->db = new PDORepository();
    }

    public function getDataTables()
    {
        $sql = 'SHOW TABLES';
        return $this->db->getData($sql);

    }

    public function getAllAuthor($sortBy = '')
    {
        $auth = new Auth();
        if (empty($auth->getId())) {
            return array();
        }

        $sql = "SELECT *,task.id as id, user.login as `user`, user_as.login as assigned_user FROM $this->table 
JOIN user ON($this->table.user_id = user.id) JOIN user as user_as ON($this->table.assigned_user_id = user_as.id)
WHERE $this->table.user_id = '" . $auth->getId() . "'";
        if(!empty($sortBy)) {
            $sql .= " ORDER BY $sortBy";
        }
        return $this->db->getData($sql);
    }

    public function getAllAssigned()
    {
        $auth = new Auth();
        if (empty($auth->getId())) {
            return array();
        }

        $sql = "SELECT *,task.id as id, user.login as `user`, user_as.login as assigned_user FROM $this->table 
LEFT JOIN user ON($this->table.user_id = user.id) LEFT JOIN user as user_as ON($this->table.assigned_user_id = user_as.id)
WHERE $this->table.assigned_user_id = '" . $auth->getId() . "' AND $this->table.user_id != '" . $auth->getId() . "'";

        return $this->db->getData($sql);
    }

    public function add($description)
    {
        $description = strip_tags($description);
        if(empty($description)) return false;

        $toDay = date('Y-m-d H:i:s');
        $user = new Auth();
        $sql = "INSERT INTO $this->table (`description`,`date_added`, `user_id`, `assigned_user_id`) VALUES (:description,:toDay, :userId, :userId)";

        return $this->db->insertData($sql, ['description' => $description, 'toDay' => $toDay, 'userId' => $user->getId()]);
    }

    public function done($id)
    {
        $sql = "UPDATE $this->table SET is_done = 1 WHERE id = $id";

        return $this->db->updateData($sql);
    }

    public function delete($id)
    {
        $sql = "DELETE from $this->table WHERE id = $id";

        return $this->db->deleteData($sql);
    }

    public function edit($id)
    {
        $id = (int)$id;
        if (empty($id)) return;
        $task = self::getTaskId($id);
        if(!empty($task[0]['description'])) {
            return $task[0]['description'];
        }
    }

    public function getTaskId($id)
    {
        return $this->db->getData("SELECT * FROM $this->table WHERE id = $id");
    }

    public function editData($id, $description)
    {
        $id = (int)$id;
        if (empty($id)) return false;

        $description = strip_tags($description);
        if(empty($description)) return false;

        $sql = "UPDATE $this->table SET description = '$description' WHERE id = $id";

        return $this->db->updateData($sql);
    }

    public function assign($idTask, $idUser)
    {
        $idTask = (int) $idTask;
        $idUser = (int) $idUser;
        $sql = "UPDATE $this->table SET assigned_user_id = $idUser WHERE id = $idTask";

        return $this->db->updateData($sql);
    }
}