<?php
require_once 'autoloader.php';

$auth = new Auth();

if(!$auth->isAuthorize())
{
    echo '<a href="register.php">Войдите на сайт</a>';
    die;
}

$ormTask = new Task();
$sortBy = '';

if (isPost()) {
    if (!empty($_POST['save'])) {
        $ormTask->add($_POST['description']);
    }

    if (!empty($_POST['edit'])) {
        $ormTask->editData($_GET['id'], $_POST['description']);
        header('Location: index.php');
    }

    if (!empty($_POST['sort'])) {
        $sortBy = $_POST['sort_by'];
    }

    if (!empty($_POST['assign'])) {
        $data = explode('_', $_POST['assigned_user_id']);
        $ormTask->assign($data[3], $data[1]);
    }
}

if (!empty($_GET['id']) && !empty($_GET['action'])) {
    switch ($_GET['action']) {
        case 'done':
        case 'delete':
            $ormTask->{$_GET['action']}($_GET['id']);
            header('Location: index.php');
            break;
        case 'edit':
            $edit = $ormTask->{$_GET['action']}($_GET['id']);
            break;
    }
}

$user = new User();

?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset=utf-8">
        <title>Задачи</title>

        <style>
            form {
                margin-bottom: 1em;
            }

            form {
                display: block;
                margin-top: 0em;
            }
            table {
                border-spacing: 0;
                border-collapse: collapse;
            }

            table td, table th {
                border: 1px solid #ccc;
                padding: 5px;
            }

            table th {
                background: #eee;
            }
        </style>

    </head>
    <body>
    <h1>Здравствуйте, <?php echo $auth->getLogin(); ?>! Вот ваш список дел:</h1>
    <div style="float: left; margin-bottom: 20px;">
        <form method="POST">
            <input type="text" name="description" placeholder="Описание задачи" value="<?php echo (isset($edit) ? $edit : '') ?>" />
            <input type="submit" name="<?php echo (isset($edit) ? 'edit' : 'save') ?>" value="<?php echo (isset($edit) ? 'Редактировать' : 'Сохранить') ?>" />
        </form>
    </div>
    <div style="float: left; margin-left: 20px;">
        <form method="POST">
            <label for="sort">Сортировать по:</label>
            <select name="sort_by">
                <option value="date_added">Дате добавления</option>
                <option value="is_done">Статусу</option>
                <option value="description">Описанию</option>
            </select>
            <input type="submit" name="sort" value="Отсортировать" />
        </form>
    </div>
    <div style="clear: both"></div>

    <table>
        <tr>
            <th>Описание задачи</th>
            <th>Дата добавления</th>

            <th>Статус</th>
            <th>Действия</th>
            <th>Ответственный</th>
            <th>Автор</th>
            <th>Закрепить задачу за пользователем</th>
        </tr>
        <tr>
            <?php if (!empty($tasks = $ormTask->getAllAuthor($sortBy))):
            foreach ($tasks as $task): ?>
        <tr>
            <td><?php echo $task['description'];?></td>
            <td><?php echo $task['date_added'];?></td>
            <?php if ($task['is_done'] == 1){ ?>
                <td><span style='color: green;'>Выполнено</span></td>
            <?php }else{?>
                <td><span style='color: orange;'>В процессе</span></td>
            <?php }?>
            <td>
                <a href='?id=<?php echo $task['id'];?>&action=edit'>Изменить</a>
                <a href='?id=<?php echo $task['id'];?>&action=done'>Выполнить</a>
                <a href='?id=<?php echo $task['id'];?>&action=delete'>Удалить</a>
            </td>
            <td><?php echo ($task['assigned_user_id'] == $auth->getId() ? 'Вы' :  $task['assigned_user']);?></td>
            <td><?php echo $task['user'];?></td>
            <td><form method='POST'>
                    <select name='assigned_user_id'>
                        <?php if (!empty($users = $user->getAll())):
                        foreach ($users as $val): ?>
                        <option value='user_<?php echo $val['id'];?>_task_<?php echo $task['id'];?>'><?php echo $val['login']?></option>
                        <?php endforeach;endif;?>
                        </select>
                    <input type='submit' name='assign' value='Переложить ответственность' />
                </form>
            </td>
        </tr>
        <?php endforeach;endif;?>
    </table>
    <p><strong>Также, посмотрите, что от Вас требуют другие люди:</strong></p>


    <table>
        <tr>
            <th>Описание задачи</th>
            <th>Дата добавления</th>

            <th>Статус</th>
            <th>Действия</th>
            <th>Ответственный</th>
            <th>Автор</th>
        </tr>
        <?php if (!empty($tasks = $ormTask->getAllAssigned($sortBy))):
        foreach ($tasks as $task): ?>
        <tr>
            <td><?php echo $task['description'];?></td>
            <td><?php echo $task['date_added'];?></td>
            <?php if ($task['is_done'] == 1){ ?>
                <td><span style='color: green;'>Выполнено</span></td>
            <?php }else{?>
                <td><span style='color: orange;'>В процессе</span></td>
            <?php }?>
            <td>
                <a href='?id=<?php echo $task['id'];?>&action=edit'>Изменить</a>
                <a href='?id=<?php echo $task['id'];?>&action=done'>Выполнить</a>
                <a href='?id=<?php echo $task['id'];?>&action=delete'>Удалить</a>
            </td>
            <td><?php echo $task['assigned_user'];?></td>
            <td><?php echo $task['user'];?></td>
        </tr>
        <?php endforeach;endif;?>
    </table>
    <p><a href="logout.php">Выход</a></p>

    </body>
</html>