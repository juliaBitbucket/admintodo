<?php
class Auth {

    private $user;

    public function __construct()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        $this->user = new User();
    }

    public function register($login, $password)
    {
        if (count($this->user->getUserLogin($login)) > 0) {
            throw new Exception('Такой пользователь уже существует в базе данных.');
        }

        $this->user->add($login, $password);
    }

    public function login($login, $password)
    {
        if (empty($login) || empty($password)) {
            throw new Exception('Не удалось залогинеться.');
        }

        $user = $this->user->getUserLogin($login);

        if (empty($user[0]) || $user[0]['password'] != md5($password)) {
            throw new Exception('Такой пользователь не существует, либо неверный пароль.');
        }


        $_SESSION['login'] = $user[0]['login'];
        $_SESSION['id'] = $user[0]['id'];
    }

    public function isAuthorize()
    {
        if (isset($_SESSION['login'])) {
            return true;
        }
        return false;
    }

    public function getId()
    {
        return $_SESSION['id'];
    }

    public function getLogin()
    {
        return $_SESSION['login'];
    }

    public function logout()
    {
        $_SESSION = array();
    }
}