<?php
require_once 'autoloader.php';

    $message = 'Введите данные для регистрации или войдите, если уже регистрировались:';
    if (isPost()) {
        if (!empty($_POST['register'])) {
            if (empty($_POST['login']) || empty($_POST['password'])) {
                $message = 'Ошибка регистрации. Введите все необхдоимые данные.';
            } else {
                try {
                    $user = new Auth();
                    $user->register($_POST['login'], $_POST['password']);
                    $user->login($_POST['login'], $_POST['password']);
                    header('Location: index.php');
                }
                catch (Exception $e) {
                    $message = $e->getMessage();
                }
            }
        } else {
            if (empty($_POST['login']) || empty($_POST['password'])) {
                $message = 'Ошибка входа. Введите все необхдоимые данные.';
            } else {
                try {
                    $user = new Auth();
                    $user->login($_POST['login'], $_POST['password']);
                    header('Location: index.php');
                }
                catch (Exception $e) {
                    $message = $e->getMessage();
                }
            }
        }

    }
?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset=utf-8">
        <title>Задачи</title>
    </head>
    <body>

    <p><?php echo $message; ?></p>

    <form method="POST">
        <input type="text" name="login" placeholder="Логин" />
        <input type="password" name="password" placeholder="Пароль" />
        <input type="submit" name="sign_in" value="Вход" />
        <input type="submit" name="register" value="Регистрация" />
    </form>
    </body>
</html>