<?php
class User {

    private $db;
    private $table = 'user';

    public function __construct()
    {
        $this->db = new PDORepository();
    }

    public function getUserLogin($login)
    {
        $login = strip_tags($login);
        $sql = "SELECT * FROM $this->table WHERE login = '$login'";
        return $this->db->getData($sql);
    }
    
    public function add($login, $password)
    {
        $login = strip_tags($login);
        $password = md5($password);
        if(empty($login)) {
            throw new Exception('Не удалось создать пользователя.');
        }

        $sql = "INSERT INTO $this->table (`login`,`password`) VALUES (:login,:password)";

        return $this->db->insertData($sql, ['login' => $login, 'password' => $password]);
    }

    public function getAll()
    {
        $sql = "SELECT * FROM $this->table";
        return $this->db->getData($sql);
    }
}